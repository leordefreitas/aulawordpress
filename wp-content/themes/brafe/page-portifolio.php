<?php
  // Template Name: Portifolio
?>

<?php get_header(); 
  // para criar o botao para muadr de pagina e conseguir acessar os 
  // outros posts que nao foram mostrados
  $paged = get_query_var('page', 1);


  // ferramenta de pesquisa dos posts 
  $news = new WP_Query(
    array(
      'posts_per_page' => 4,
      'paged' => $paged,
      'post_type' => "noticia",
      'post_status' => 'publish',
      'suppress_filters' => true,
      'orderby' => 'post_date',
      'order' => 'DESC'
    )
  );
?>

<h1 class="tst">Portifolio</h1>

<!-- ferramenta de busca no wordpress duas formas de fazer, segunda e melhor para fazer separado as pesquisas-->
<?php // get_search_form(); ?>
<div>
  <h3>Procurar noticias</h3>
  <form action="<?php echo site_url('/'); ?>" method="get" role="search" id="searchform">
    <input type="text" name="s" placeholder="Pesquisa noticias"/>
    <input type="hidden" name="post_type" value="noticia"/>
    <input type="submit" alt="Search" value="Search"/>
  </form>
</div>


<!-- funcao padrao para colocar os posts -->
<?php if($news -> have_posts()):
  while($news -> have_posts()):
      $news -> the_post(); ?>
      <div id="pg-portfolio">
        <h2><?php the_title(); ?></h2>
        <!-- this funciton do php e para que indique o link -->
        <a href="<?php the_permalink(); ?>">Link para a noticia</a>
        <p><?php echo wp_strip_all_tags(the_content()); the_date(); ?></p>
      </div>
  <?php endwhile;
else:?>
  <p>Nao temos noticias.</p>
<?php endif; ?>

<div id="menu-post">
  <?php
    // criar os botoes para acessar as outras paginas do post, copiar isso quando quiser criar uma igual
    $big = 9999;
    echo paginate_links(array(
      'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
      'format' => '?paged=%#%',
      'current' => max(1 , get_query_var('paged') ),
      'prev_text' => __('Anterior'),
      'next_text' => __('Proximo'),
      'total' => $news->max_num_pages
    ));
    
  ?>
  
</div>

<?php 
  // so para resetar as informacoes dos posts, boa pratica coloca-la
  wp_reset_postdata(); 
?>

<?php get_footer(); ?>