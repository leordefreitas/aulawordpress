<?php
  // Template Name: Home Page
?>

<?php get_header(); ?>

<style>
  div#bck-txt-1{
    background-image: url(<?php echo get_field("imagem_de_fundo")["url"]; ?>)
  }
  @media only screen and (max-width: 760px){
    div#bck-txt-1{
    background-image: url(<?php echo esc_url(get_field("imagem_de_fundo")["sizes"]["medium_large"]); ?>),
    background-size: cover
    }
  }
</style>

<div id="bck-txt-1">
  <div id="txt-1">
    <h1><?php the_field("titulo_chamativo") ?></h1>
    <p><?php the_field("sub_titulo_chamativo") ?></p>
  </div>
</div>
<div id="bck-txt-2">

<!-- CHAMAR FRAGMENTO MISTURA -->
<?php include get_template_directory().'/include/mistura.php' ?>

<!-- CHAMAR FRAGMENTO PRODUTOS -->
<?php include get_template_directory().'/include/produtos.php' ?>

</div>
<div id="bck-txt-4">
  <div class="pdr-img-txt-1">
    <img src="<?php the_field("primeira_loja_imagem") ?>" alt="cafe-2">
    <div class="img-txt-1">
      <h1><?php the_field("primeira_loja_titulo") ?></h1>
      <p1><?php the_field("primeira_loja_texto") ?></p1>
      <div class="btn-1">
        <p>VER MAPA</p>
      </div>
    </div>
  </div>
  <div class="pdr-img-txt-1">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>./utils/Brafé/img/iguatemi.jpg" alt="cafe-3">
    <div class="img-txt-1">
      <h1>Iguatemi</h1>
      <p1>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões</p1>
      <div class="btn-1">
        <p>VER MAPA</p>
      </div>
    </div>
  </div>
  <div class="pdr-img-txt-1">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>./utils/Brafé/img/mineirao.jpg" alt="cafe-4">
    <div class="img-txt-1">
      <h1>Mineirão</h1>
      <p1>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões</p1>
      <div class="btn-1">
        <p>VER MAPA</p>
      </div>
    </div>
  </div>
</div>
<div id="cnt-eml-1">
  <div id="cnt-1">
    <h1>Assine Nossa Newsletter</h1>
    <p>promoções e eventos mensais</p>
  </div>
  <div class="eml-1">
    <input type="text" placeholder="Digite seu email">
    <div class="btn-2">
      <p class="btn-env-1">ENVIAR</p>
    </div>
  </div>
</div>

<?php get_footer(); ?>
