<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php 

if(is_front_page()){
  echo wp_title("");
}elseif(is_page()){
  echo wp_title(); echo ' - ';
}elseif(is_search()){
  echo 'Busca - ';
}elseif(!(is_404()) && (is_single()) || (is_page())){
  echo wp_title(); echo ' - ';
}elseif(is_404()){
  echo 'Nao encontrada';
} bloginfo("name")

?></title>

<?php wp_head(); ?>

</head>
<body>
  <header>
    <div id="hdr-esq">
      <a href="<?php if(is_front_page()){echo "#";}else{echo get_home_url();} ?>">Brafé</a>
    </div>
    <div id="hdr-dir">
      <?php 
        $args = array(
          'menu' => 'principal',
          'container' => false
        );
        wp_nav_menu($args);
      ?>
    </div>
  </header>
  <div id="hdr"></div>