<div id="txt-2">
    <h1>Uma Mistura de</h1>
    <div id="dv-img-1">
      <div id="dv-img-txt-1">
        <?php echo wp_get_attachment_image( get_field("primeira_imagem_mistura")["id"], "bg-desktop"); ?>
        <div>
          <p><?php the_field("primeiro_texto_mistura") ?></p>
        </div>
      </div>
      <div id="dv-img-txt-2">
        <img src="<?php the_field("segunda_imagem_mistura") ?>" alt="">
        <div>
          <p><?php the_field("segundo_texto_mistura") ?></p>
        </div>
      </div>
    </div>
    <p id="txt-p-1"><?php the_field("texto_principal_mistura") ?></p>
  </div>