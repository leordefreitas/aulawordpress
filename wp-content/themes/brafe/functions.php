<?php 

// para criar um console.log no php
function console_log($data){
  echo '<script>';
  echo 'console.log('. json_encode($data) .')';
  echo '</script>';
}


add_theme_support("menus");

add_image_size("bg-desktop", 1000, 800, true);

function brafe_scripts() {
  // caso queira usar as propriedades doo editor de texto do wordpress
  // e nao utilizar o css para formatar o texto que vier do post
  // if (!is_page("portifolio")){
  // }
  wp_enqueue_style( "reset-sheet", get_stylesheet_directory_uri() . "/css/reset.css" );
  wp_enqueue_style( "style-sheet", get_stylesheet_directory_uri() . "/style.css" );
}

add_action( 'wp_enqueue_scripts', 'brafe_scripts' );


// para pegar os postes e lidar com eles, criar os posts e como ele
// vai ficar no menu
function custom_post_type() {
  $labels = array(
    'name' => _x('Notícias', 'post type general name'),
    'singular_name' => _x('Notícia', 'post type singular name'),
    'add_new' => _x('Adicionar Novo', 'Novo item'),
    'add_new_item' => __('Novo Item'),
    'edit_item' => __('Editar Item'),
    'new_item' => __('Novo Item'),
    'view_item' => __('Ver Item'),
    'search_items' => __('Procurar Itens'),
    'not_found' =>  __('Nenhum registro encontrado'),
    'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
    'parent_item_colon' => '',
    'menu_name' => 'Notícias'
);

$args = array(
  'label' => 'Noticia',
  'description' => 'Noticias',
  'menu_position' => 3,
  'public' => true,
  'show_ui' => true,
  'show_in_menu' => true,
  'map_meta_cap' => true,
  'labels' => $labels,
  'public_queryable' => true,
  'query_var' => true,
  'rewrite' => true,
  'capability_type' => 'post',
  'has_archive' => true,
  'hierarchical' => false,
  'supports' => array('title','editor')
  );

register_post_type( 'noticia' , $args );

}
//  init e para o comeco do site, quando inicia o wordpress e o outro e a funcao que 
// eu quero que funcione
add_action('init', 'custom_post_type');

// funcao da parte de pesquisa, nao faco a menor ideia para que serve
function tg_include_custom_post_types_in_search_results($query) {
  if ( $query->is_main_query() && $query->is_search() && ! is_admin() ) {
    $query->set( 'post_type', array( 'noticia', 'posts', 'pages' ));
  }
}
add_action('pre_get_posts', 'tg_include_custom_post_types_in_search_results');

?>