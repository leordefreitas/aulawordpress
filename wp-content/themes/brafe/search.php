<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

	<div>
		<main>

		<?php if ( have_posts() ) : ?>

				<h1>
					<?php _e( 'Search results for: ', 'twentynineteen' ); ?>
					<span class="page-description"><?php echo get_search_query(); ?></span>
				</h1>

			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that
				 * will be used instead.
				 */
				// get_template_part( 'template-parts/content/content', 'excerpt' );
        ?>

       <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

        <?php
				// End the loop.
			endwhile;

      // isso e para ter paginacao nos links de pesquisa igual dos posts
      echo paginate_links();

			// If no content, include the "No posts found" template.
		else :
			// get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
