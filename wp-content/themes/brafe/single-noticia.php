<?php 
  // Template Name: noticia
?>

<?php get_header(); ?>

<main id="single-noticia" class="container">
  <div class="card">
    <div>
      <h1><?php the_title(); ?></h1>
      <div>
        <p>Criado em: <?php echo get_the_date(); ?></p>
        <p>Editado em: <?php echo get_the_modified_date(); ?></p>
      </div>
    </div>
    <?php
    // sempre tem que ter o while para exibir o the_content()
      while (have_posts()):
        the_post();
        the_content();
      endwhile;
    ?>
  </div>
</main>

<?php get_footer(); ?>