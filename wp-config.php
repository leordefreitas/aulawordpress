<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'laGoqds2umqgYJ2ZUrhUwJSyB7EKtsz3qhCfYrAHtqj29l7sO7hA3ImLTzLCp85Apw6w8c0BZbVDZIW/7bAw5w==');
define('SECURE_AUTH_KEY',  'i1FOYyqbRz19SQePQnigv7Ux8Gv71uSNxoWYgceyPIJYQQG5z6mmqr9mJsWGCPAZqF38f9UVf7eWUQI4o6O3dA==');
define('LOGGED_IN_KEY',    '5gBSkkDb6+3P4TZmTHZkBvdxGAzlmPpH3OTM/5eoBfrWL2bGvxEYt9soG22DL3u4NTMcqCfh9dV4PoBH3PYjtA==');
define('NONCE_KEY',        'UFRoSzygxKpA1N3WUpigs4o8R4eBMGIHLa5Kl4k8xTsxndv0QMlKmL92BAWago1UO5DqXnZX+DXpNPDJoR1pSA==');
define('AUTH_SALT',        'm+byDxQ44QKT2BvLF9BUOjfgCgYU2/pemacFSTBdXOwTn+Mf1VFN/52jh8t+IWu4seAfoaob5peVZ/T+ydDvgA==');
define('SECURE_AUTH_SALT', 'HZeVT9Bmrn4+j4joj8jF+XymImumKakcc2bffPTfeRRAorVpupW2Z+eDETdyasbJ6pzjvaScJgVUBntvoGJRGQ==');
define('LOGGED_IN_SALT',   'TbQKwsEL/O6yj7uc10Ao2x3RcznAQxdRN9+Qu95mVNtn2x1TlM3pNVHcuUVwZoOQS32E+sxR0IQhams3gQBv0g==');
define('NONCE_SALT',       'o+rjbkIYmvFnNrx7nWOGadP/sYNoUx6BHdJYEFUJLnFuSKN8g085zxS5FmukoJJf75QJ/7vpJ1TQDHuN01PiWA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
